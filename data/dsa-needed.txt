A DSA is needed for the following source packages in old/stable. The specific
CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

Some packages are not tracked here:
- Linux kernel (tracking in kernel-sec repo)
- Embargoed issues continue to be tracked in separate file.

To pick an issue, simply add your uid behind it.

If needed, specify the release by adding a slash after the name of the source package.

--
chromium
--
knot-resolver
  Santiago Ruano Rincón proposed a debdiff for review
--
linux (carnil)
  Wait until more issues have piled up, though try to regulary rebase for point
  releases to more recent v4.19.y versions.
--
netty
  Markus Koschany possibly can prepare update
--
php7.3
  Maintainer proposed an update via 7.3.27
--
python-pysaml2
--
screen
  Maintainer (abe) will take care
--
spip (seb)
  Maintainer prepared updates.
--
xcftools
  Hugo proposed to work on this update
--
